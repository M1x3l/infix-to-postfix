# infix-to-postfix

## Table of Contents

- [infix-to-postfix](#infix-to-postfix)
	- [Table of Contents](#table-of-contents)
	- [Requirements](#requirements)
	- [Usage](#usage)
	- [Examples](#examples)
	- [Valid input](#valid-input)
		- [Numbers](#numbers)
		- [Letter variables](#letter-variables)
		- [Operations](#operations)
		- [Parentheses](#parentheses)
	- [Output](#output)

## Requirements

You need to have the following things installed on your machine:

- [Node](https://nodejs.org)

## Usage

- ### With bash installed

  You just need to **run the `run.sh` file**

- ### Without bash installed

  You need to run the script like this:

  ```console
  node built/index.js '<infix expression>'
  ```

## Examples

You need to separate every Number or Operation ([list of operations](#operations)) by a <kbd>SPACE</kbd> for them to be recognised correctly, if not separated the expression _will_ produce unwanted outputs

```console
# with bash:
./run.sh '1 + 2 - 3'

# without bash:
node built/index.js '1 + 2 - 3'
```

## Valid input

### Numbers

Positive and negative can be used.

### Letter variables

Any word consisting of only letters from `a` to `z` can be used.

### Operations

|  Operation  | Meaning                                                                    | Precedence |
| :---------: | :------------------------------------------------------------------------- | :--------: |
|     `+`     | **Plus**, adds two numbers                                                 |     1      |
|     `-`     | **Minus**, subtracts a number from another one                             |     1      |
|     `*`     | **Times**, multiplies two numbers                                          |     2      |
|     `/`     | **Divided by**, divides a number by another one                            |     2      |
| `^` or `**` | **To the power of**, raises a number by another one                        |     3      |
|     `!`     | **Factorial**, multiplies a number by all it's preceding numbers down to 1 |     4      |

### Parentheses

You can use parentheses to changes the order of operations.

## Output

**If successful**, the example above will print a postfix representation of the term `1 + 2 - 3`, which in this case would result to `1 2 + 3 -`

**If unsuccessful**, the script will return with a non zero exit code
