export type Operation = '+' | '-' | '*' | '/' | '^' | '!' | 'INVALID';

export enum Types {
	NO_TYPE,
	OPERAND,
	OPERATION,
	PARENTHESIS,
}

export class Stack {
	private stack: string[] = [];

	pop() {
		return this.stack.pop();
	}

	push(element: string) {
		this.stack.push(element);
		return this;
	}

	top() {
		return this.stack[this.stack.length - 1];
	}

	length() {
		return this.stack.length;
	}

	/** For Debugging purposes */
	print(stackName?: string) {
		console.log(
			`${stackName || 'Stack'}: ${JSON.stringify(this.stack, null, 2)}`
		);
	}
}
