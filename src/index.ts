import { Operation, Stack, Types } from './Util';

const args: string[] = process.argv[process.argv.length - 1]
	.split(' ')
	.map((e) => e);

const output = [];
const postfixOperationStack = new Stack();

for (let arg of args) {
	if (checkOPType(arg, Types.OPERAND)) output.push(arg);
	else if (arg == '(') postfixOperationStack.push('(');
	else if (arg == ')') {
		while (postfixOperationStack.top() != '(') {
			output.push(postfixOperationStack.pop()!);
		}
		postfixOperationStack.pop();
	} else {
		while (
			postfixOperationStack.top() &&
			precedence(arg) <= precedence(postfixOperationStack.top()!)
		) {
			output.push(postfixOperationStack.pop()!);
		}
		postfixOperationStack.push(arg);
	}
}
while (postfixOperationStack.top()) output.push(postfixOperationStack.pop()!);

console.log(output.join(' '));

function checkOPType(op: string, type: Types): boolean {
	const patterns = {
		OPERAND: /^((-?\d+)|([a-zA-Z]+))$/,
		OPERATION: /^(\+|\-|\*|\/|\^|\*\*|!)$/,
		PARENTHESIS: /^[()]$/,
	};

	let testedType: Types;
	if (patterns.OPERAND.test(op)) testedType = Types.OPERAND;
	else if (patterns.OPERATION.test(op)) testedType = Types.OPERATION;
	else if (patterns.PARENTHESIS.test(op)) testedType = Types.PARENTHESIS;
	else testedType = Types.NO_TYPE;
	return type == testedType;
}

function precedence(op: string): number {
	const precedenceMap: Map<Operation, number> = new Map()
		.set('+', 1)
		.set('-', 1)
		.set('*', 2)
		.set('/', 2)
		.set('^', 3)
		.set('**', 3)
		.set('!', 4);
	return precedenceMap.get(op as Operation) || 0;
}
