"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Util_1 = require("./Util");
var args = process.argv[process.argv.length - 1]
    .split(' ')
    .map(function (e) { return e; });
var output = [];
var postfixOperationStack = new Util_1.Stack();
for (var _i = 0, args_1 = args; _i < args_1.length; _i++) {
    var arg = args_1[_i];
    if (checkOPType(arg, Util_1.Types.OPERAND))
        output.push(arg);
    else if (arg == '(')
        postfixOperationStack.push('(');
    else if (arg == ')') {
        while (postfixOperationStack.top() != '(') {
            output.push(postfixOperationStack.pop());
        }
        postfixOperationStack.pop();
    }
    else {
        while (postfixOperationStack.top() &&
            precedence(arg) <= precedence(postfixOperationStack.top())) {
            output.push(postfixOperationStack.pop());
        }
        postfixOperationStack.push(arg);
    }
}
while (postfixOperationStack.top())
    output.push(postfixOperationStack.pop());
console.log(output.join(' '));
function checkOPType(op, type) {
    var patterns = {
        OPERAND: /^((-?\d+)|([a-zA-Z]+))$/,
        OPERATION: /^(\+|\-|\*|\/|\^|\*\*|!)$/,
        PARENTHESIS: /^[()]$/,
    };
    var testedType;
    if (patterns.OPERAND.test(op))
        testedType = Util_1.Types.OPERAND;
    else if (patterns.OPERATION.test(op))
        testedType = Util_1.Types.OPERATION;
    else if (patterns.PARENTHESIS.test(op))
        testedType = Util_1.Types.PARENTHESIS;
    else
        testedType = Util_1.Types.NO_TYPE;
    return type == testedType;
}
function precedence(op) {
    var precedenceMap = new Map()
        .set('+', 1)
        .set('-', 1)
        .set('*', 2)
        .set('/', 2)
        .set('^', 3)
        .set('**', 3)
        .set('!', 4);
    return precedenceMap.get(op) || 0;
}
