"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Types;
(function (Types) {
    Types[Types["NO_TYPE"] = 0] = "NO_TYPE";
    Types[Types["OPERAND"] = 1] = "OPERAND";
    Types[Types["OPERATION"] = 2] = "OPERATION";
    Types[Types["PARENTHESIS"] = 3] = "PARENTHESIS";
})(Types = exports.Types || (exports.Types = {}));
var Stack = /** @class */ (function () {
    function Stack() {
        this.stack = [];
    }
    Stack.prototype.pop = function () {
        return this.stack.pop();
    };
    Stack.prototype.push = function (element) {
        this.stack.push(element);
        return this;
    };
    Stack.prototype.top = function () {
        return this.stack[this.stack.length - 1];
    };
    Stack.prototype.length = function () {
        return this.stack.length;
    };
    /** For Debugging purposes */
    Stack.prototype.print = function (stackName) {
        console.log((stackName || 'Stack') + ": " + JSON.stringify(this.stack, null, 2));
    };
    return Stack;
}());
exports.Stack = Stack;
